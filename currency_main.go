package main

import (
	"fmt"

	"bitbucket.com/robb-randall/coindesk/currencies"
)

func main() {
	for _, currency := range currencies.Supported() {
		fmt.Printf("%s - %s\n", currency.Currency, currency.Country)
	}
}
