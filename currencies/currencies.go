package currencies

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type response []country

type country struct {
	Currency string `json:"currency"`
	Country  string `json:"country"`
}

// TODO: Handle errors
func Supported() (r response) {
	url := "https://api.coindesk.com/v1/bpi/supported-currencies.json"

	resp, _ := http.Get(url)
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &r)
	return r
}
