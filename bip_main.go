package main

import (
	"fmt"

	"bitbucket.com/robb-randall/coindesk/bpi"
)

func main() {
	btcChannel := bpi.Start("btc")

	for btc := range btcChannel {
		fmt.Println(fmt.Sprintf("%s -- $%s", btc.Time.Updated, btc.BPI["USD"].Rate))
	}

}
