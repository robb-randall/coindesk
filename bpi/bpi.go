package bpi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type response struct {
	Time       responseTime `json:"time"`
	Disclaimer string       `json:"disclaimer"`
	BPI        responseBpi  `json:"bpi"`
}

type responseTime struct {
	Updated    string `json:"updated"`
	UpdatedISO string `json:"updatedISO"`
	Updateduk  string `json:"updateduk"`
}

type responseBpi map[string]responseBpiCurrency

type responseBpiCurrency struct {
	Code        string  `json:"code"`
	Rate        string  `json:"rate"`
	Description string  `json:"description"`
	RateFloat   float32 `json:"rate_float"`
}

// Returns a function that will call CoinDesk's BPI API every call
// Please note that the data is only updated every minute and not every call will result in updated price.
// TODO: Handle errors
func Get(currency string) func() response {
	url := fmt.Sprintf("https://api.coindesk.com/v1/bpi/currentprice/%s.json", currency)

	return func() (r response) {
		resp, _ := http.Get(url)
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)
		json.Unmarshal(body, &r)
		return r
	}
}

func Start(currency string) chan response {
	channel := make(chan response)
	go start(currency, channel)
	return channel
}

func start(currency string, channel chan response) {
	bpiCurrency := Get(currency)
	for {
		channel <- bpiCurrency()
		time.Sleep(time.Minute * 1)
	}
}
